var Helper = {
    Filters: {
        FilterCounter: 0,
        Initialize: function(index) {
            for (var i = 0; i < tables[index].fields.length; i++) {
                if (tables[index].fields[i].activeAsFilter)
                    Helper.Filters.AddFilterRow(tables[index].fields[i]);
            }
        },
        SetFilterCounter: function(number) {
            this.FilterCounter = number;
        },
        AddFilterRow: function(filter) {
            if (Helper.Globals.IsEmpty(filter)) {
                filter = {
                    name: "",
                    description: ""
                };
            }
            Helper.Filters.FilterCounter++;
            var filterName = "filter_name_" + Helper.Filters.FilterCounter.toString();
            var filterValue = "filter_value_" + Helper.Filters.FilterCounter.toString();
            var filterCriteria = "filter_criteria_" + Helper.Filters.FilterCounter.toString();
            var criteriaTitle = "IN: Incluido en\nEQ: Igual a\nGT: Mayor que\nGE: Mayor o igual que\nLT: Menor que\nLE: Menor o igual que\nLIKE: Comodin";
            var valueTitle = "Fecha: AAAAMMDD\nMonto: 1234.50\nComodin: %valor%";
            var text = '<td>' + Helper.Filters.FilterCounter + '</td><td><input class="form-control" id="' + filterName + '" style="text-align: center; width: 90px" type="text" \
                            onfocusout="Helper.Filters.Events.OnFocusOut(\'' + filterName + '\',' + Helper.Filters.FilterCounter.toString() + ')"\
                            value="' + filter.name + '" title="' + filter.description + '"></td>\
                        <td><input class="form-control" id="' + filterValue + '" style="text-align: center; width: 180px" type="text" \
                        title="' + valueTitle + '" onkeypress="Helper.Filters.Events.OnKeyPress(event)"> \
                        </td><td style="text-align: left;"><select class="form-control" id="' + filterCriteria + '" \
                        style="text-align: center; width: 80px;" type="text" value="IN"\
                        title="' + criteriaTitle + '" >\
                        <option value="IN">IN</option>\
                        <option value="EQ">EQ</option>\
                        <option value="GT">GT</option>\
                        <option value="GE">GE</option>\
                        <option value="LT">LT</option>\
                        <option value="LE">LE</option>\
                        <option value="LIKE">LIKE</option>\
                        </select>\
                        </td>';
            var tr = document.createElement('tr');
            tr.innerHTML = text.trim();
            document.getElementById("filters_rows").appendChild(tr);
            document.getElementById(filterName).focus();
        },
        Clear: function(empty){
            document.getElementById("filters_rows").innerHTML = "";
            document.getElementById("combined_filters").value = "";
            Helper.Filters.SetFilterCounter(0);
            if(!empty) Helper.Filters.AddFilterRow();

        },
        URLFilter:
        {
            GetFilter: function(filters, combinedFilters)
            {
                builtFilters = [];
                filters.forEach(function(item, index){
                    if(Helper.Globals.IsEmpty(item.field) ) return;
                    else if(Helper.Globals.IsEmpty(item.value) && item.criteria != "EQ" ) return;

                    switch(item.criteria)
                    {
                        case "IN":
                            var currentIndex = index + 1;
                            var inFilter = { value: "", number: currentIndex.toString()};
                            inFilter.value += item.field + "+IN+(_placeholder_)";
                            item.value = item.value.replace(/\|/gm,",");
                            item.value = item.value.replace(/\ /gm,",");
                            var inValues = item.value.split(",");
                            var inText = "";
                            inValues.forEach(function(inItem, inIndex){
                                inItem = "'" + inItem + "'";
                                inText += inItem;
                                inText += inIndex != (inValues.length - 1) ? "," : "";
                            });
                            inFilter.value = inFilter.value.replace("_placeholder_", inText);
                            builtFilters.push(inFilter);
                        break;
                        case "EQ": case "GT": case "GE": case "LT": case "LE":
                            var currentIndex = index + 1;    
                            var uniqueValueFilter = { value: "", number: currentIndex.toString()};
                            uniqueValueFilter.value = item.field + "+" + item.criteria + "+'" + item.value + "'";
                            builtFilters.push(uniqueValueFilter);
                        break;
                        case "LIKE":
                            var currentIndex = index + 1;    
                            var likeFilter = { value: "", number: currentIndex.toString()};
                            likeFilter.value = item.field + "+LIKE+'" + item.value.replace(/\*/gm,"%") + "'";
                            builtFilters.push(likeFilter);
                        break;
                    }
                });

                combinedFilters =  combinedFilters.toLowerCase();

                for(var i = 1; i <= 9; i++)
                {
                    combinedFilters = combinedFilters.replace(i.toString(), "#" + i.toString() + "#");
                }

                combinedFilters = combinedFilters.replace(/\s/gm,"");
                combinedFilters = combinedFilters.replace(/y|\&/gm,"+AND+");
                combinedFilters = combinedFilters.replace(/no|\!/gm,"+NOT+");
                combinedFilters = combinedFilters.replace(/o|\|/gm,"+OR+");
                combinedFilters = combinedFilters.replace(/\+\+/gm,"+");
                var noCombinedFilters = Helper.Globals.IsEmpty(combinedFilters);
                builtFilters.forEach(function(item, index){
                    var currentNumber = item.number;
                    if(!noCombinedFilters)
                    {
                        combinedFilters = combinedFilters.replace("#" + currentNumber.toString() + "#", item.value);
                    }
                    else{

                        combinedFilters += index != 0 ? "+AND+" : "";
                        combinedFilters += item.value;
                    }
                });

                combinedFilters = combinedFilters.replace(/\%/gm,"%25");

                return combinedFilters;
            },
        },
        Events: {
            OnKeyPress: function(event) {
                if (event.key === "Enter") {
                    Helper.Filters.AddFilterRow();
                    return false;
                }
                return true;
            },
            OnFocusOut: function(id, index) {
                var flag = false;
                document.getElementById(id.toString()).value = document.getElementById(id.toString()).value.toUpperCase();
                for (var i = 0; i < tables[Helper.Tables.CurrentTableIndex].fields.length; i++) {
                    if (tables[Helper.Tables.CurrentTableIndex].fields[i].name.toLowerCase() == document.getElementById(id.toString()).value.toLowerCase()) {
                        document.getElementById(id.toString()).title = tables[Helper.Tables.CurrentTableIndex].fields[i].description;
                        flag = true;
                    }
                }
                if (!flag) {
                    document.getElementById(id.toString()).title = "No encontrado";
                }
            }
        }
    },

    Fields: {
        FieldsCounter: 0,
        Initialize: function(index) {
            for (var i = 0; i < tables[index].fields.length; i++) {
                if (tables[index].fields[i].activeAsField)
                    Helper.Fields.AddFieldRow(tables[index].fields[i]);
            }
        },
        SetFieldsCounter: function(number) {
            this.FieldsCounter = number;
        },
        AddFieldRow: function(field) {
            if (Helper.Globals.IsEmpty(field)) {
                field = {
                    name: "",
                    description: ""
                };
            }
            Helper.Fields.FieldsCounter++;
            var fieldName = "field_name_" + Helper.Fields.FieldsCounter.toString();
            var fieldDescription = "field_description_" + Helper.Fields.FieldsCounter.toString();
            var text = '<td><input class="form-control" id="' + fieldName + '" style="text-align: left; width: 100px; display: inline" \
                        type="text" onkeypress="Helper.Fields.Events.OnKeyPress(event)" \
                        onchange="Helper.Fields.Events.OnChange()" \
                        onfocusout="Helper.Fields.Events.OnFocusOut(\'' + fieldName + '\',' + Helper.Fields.FieldsCounter.toString() + ')" \
                        value="' + field.name + '" title="' + field.description + ' \"></td>\
                        <td style="text-align: left"><small id="' + fieldDescription + '">' + field.description + '</small></td>';

            var tr = document.createElement('tr');
            tr.innerHTML = text.trim();
            document.getElementById("fields_rows").appendChild(tr);

            document.getElementById(fieldName).focus();
        },
        Clear: function(empty){
            document.getElementById("fields_rows").innerHTML = "";
            Helper.Fields.SetFieldsCounter(0);
            if(!empty) Helper.Fields.AddFieldRow();
        },
        Events: {
            OnKeyPress: function(event) {
                if (event.key === "Enter") {
                    Helper.Fields.AddFieldRow();
                    return false;
                }
                return true;
            },
            OnFocusOut: function(id, index) {
                var flag = false;
                document.getElementById(id.toString()).value = document.getElementById(id.toString()).value.toUpperCase();
                for (var i = 0; i < tables[Helper.Tables.CurrentTableIndex].fields.length; i++) {
                    if (tables[Helper.Tables.CurrentTableIndex].fields[i].name.toLowerCase() == document.getElementById(id.toString()).value.toLowerCase()) {
                        document.getElementById(id.toString() + "").title = tables[Helper.Tables.CurrentTableIndex].fields[i].description;
                        document.getElementById("field_description_" + index.toString()).innerHTML = tables[Helper.Tables.CurrentTableIndex].fields[i].description;
                        flag = true;
                    }
                }
                if (!flag) {
                    document.getElementById(id.toString() + "").title = "Campo desconocido";
                    document.getElementById("field_description_" + index.toString()).innerHTML = "";
                }
            },
            OnChange: function() {
                document.getElementById("field_name_1").style.borderColor = "#ced4da";
                document.getElementById("field_name_1").style.borderWidth = "1px";
            }
        }
    },

    Tables: {
        CurrentTableIndex: 0,
        Events: {
            OnChange: function(event) {
                Helper.Tables.CurrentTableIndex = event.selectedIndex;
                Helper.Filters.SetFilterCounter(0);
                Helper.Fields.SetFieldsCounter(0);
                document.getElementById("fields_rows").innerHTML = "";
                document.getElementById("filters_rows").innerHTML = "";
                Helper.Fields.Initialize(event.selectedIndex);
                Helper.Filters.Initialize(event.selectedIndex);
                document.getElementById("field_name_1").focus();             
            },
            OnClickNewTable: function(){
                Helper.Filters.Clear();
                Helper.Fields.Clear();
                document.getElementById("tables").style.display = "none";
                document.getElementById("btn_new").style.display = "none";
                document.getElementById("newtable").style.display = "block";
                document.getElementById("btn_existing").style.display = "block";
                document.getElementById("newtable").focus();
            },
            OnClickExisting: function(){
                Helper.Filters.Clear(true);
                Helper.Fields.Clear(true);
                document.getElementById("tables").style.display = "block";
                document.getElementById("btn_new").style.display = "block";
                document.getElementById("newtable").style.display = "none";
                document.getElementById("btn_existing").style.display = "none";
                Helper.Filters.Initialize(0);
                Helper.Fields.Initialize(0);
            },
            NewTable: {
                OnChange: function()
                {
                    document.getElementById("newtable").style.borderColor = "#ced4da";
                    document.getElementById("newtable").style.borderWidth = "1px";
                    document.getElementById("newtable").value = document.getElementById("newtable").value.toUpperCase();
                }
            }
        }
    },

    Clients: {
        CurrentClientIndex: 0,
        Events: {
            OnChange: function(event)
            {
                var html = "";
                html += "<select class='form-control' id='env_menu' style='width: 90%'>";
                if(event.selectedIndex != 0)
                {
                    var env = clients[event.selectedIndex - 1].environments;
                    for(var i = 0; i < env.length; i++)
                    {
                        html += "<option value='" + env[i].config + "'>" + env[i].name + " - " + env[i].config + "</option>";
                    }
                }
                else
                {
                    html += "<option></option>" ;
                }
                html +="</select>";
                
                document.getElementById("ambientes").innerHTML = html;
                document.getElementById("clients_menu").style.borderColor = "#ced4da";
                document.getElementById("clients_menu").style.borderWidth = "1px"; 
            }
        }
    },

    Environments: {
        Events:{
            OnClickNewEnvironment: function(){
                document.getElementById("ambientes").style.display = "none";
                document.getElementById("btn_new_env").style.display = "none";
                document.getElementById("clients_group").style.display = "none";
                document.getElementById("newenv").style.display = "block";
                document.getElementById("btn_existing_env").style.display = "block";
                document.getElementById("newenv").focus();
            },
            OnClickExisting: function(){
                document.getElementById("ambientes").style.display = "block";
                document.getElementById("btn_new_env").style.display = "block";
                document.getElementById("clients_group").style.display = "block";
                document.getElementById("newenv").style.display = "none";
                document.getElementById("btn_existing_env").style.display = "none";
            },
            OnChange: function(){
                document.getElementById("newenv").style.borderColor = "#ced4da";
                document.getElementById("newenv").style.borderWidth = "1px"; 
            }
        }
    },

    Globals: {
        IsEmpty: function(string) {
            return string == "" || string == null || string == undefined;
        }
    },

    URL: {
        BaseURL: "https://na2.esker.com/ondemand/webaccess/asc/SapQuery.ashx",
        Generate: function()
        {
            var flag = false;
            
            var config = "";
            if(document.getElementById("btn_existing_env").style.display == "none")
            {
                config = document.getElementById("env_menu").value;
                if(Helper.Globals.IsEmpty(config))
                {  
                    document.getElementById("clients_menu").style.borderColor = "#ff0000";
                    document.getElementById("clients_menu").style.borderWidth = "2.5px";
                    document.getElementById("clients_menu").focus();
                    flag = true;
                }
            }
            else
            {
                config = document.getElementById("newenv").value;
                if(Helper.Globals.IsEmpty(config))
                {  
                    document.getElementById("newenv").style.borderColor = "#ff0000";
                    document.getElementById("newenv").style.borderWidth = "2.5px";
                    document.getElementById("newenv").placeholder = "Te olvidaste de cargar esto. Ticket en la retro.";
                    flag = true;
                }
            }

            var table = "";
            if(document.getElementById("btn_existing").style.display == "none")
            {
                table = document.getElementById("table_menu").value;
            }
            else
            {
                table = document.getElementById("newtable").value;
                if(Helper.Globals.IsEmpty(table))
                {
                    document.getElementById("newtable").style.borderColor = "#ff0000";
                    document.getElementById("newtable").style.borderWidth = "2.5px";
                }
            }

            var filters = [];
            for(var i = 0; i < Helper.Filters.FilterCounter; i++)
            {
                var index = i + 1;
                var obj = {
                    field: document.getElementById("filter_name_" + index).value,
                    criteria: document.getElementById("filter_criteria_" + index).value,
                    value: document.getElementById("filter_value_" + index).value
                };

                filters.push(obj);
            }

            var fields = [];
            for(var i = 0; i < Helper.Fields.FieldsCounter; i++)
            {
                var index = i + 1;
                var field = document.getElementById("field_name_" + index).value;

                if(!Helper.Globals.IsEmpty(field))
                {
                    fields.push(field);
                }
            }

            if(Helper.Globals.IsEmpty(fields))
            {
                flag = true;
                document.getElementById("field_name_1").style.borderColor = "#ff0000";
                document.getElementById("field_name_1").style.borderWidth = "2.5px";
            }

            var combinedFilters = document.getElementById("combined_filters").value;

            var url = "";
            if(!flag)
            {
                url += "SAPCONF=" + config;
                url += "&TABLE=" + table;
                url += "&FIELDS=" + fields.join().replace(/\,/gm,"%7C");
                url += "&OPTIONS=" + Helper.Filters.URLFilter.GetFilter(filters, combinedFilters) + "";
                url += "&ROWCOUNT=200";
                url += "&ROWSKIP=0";
                url += "&NODATA=no";
                url += "&DEBUG=no";
                url += "&WITHATTRIBUTES=no";
                url += "&randomParam=0.010416454289501331";
                window.open(Helper.URL.BaseURL + "?" + url);
            }
        }
    },

    JSONProcessor: {
        TablesAdded: 0,
        DrawTable: function(rawResults){
            var jsonResults = JSON.parse(rawResults);
            var rows = [];
            var row = [];
            var arrRecordsDefinition = Object.values(jsonResults.RecordsDefinition);
            var arrRecordsDefinitionNiceNames = Object.values(jsonResults.RecordsDefinitionNiceNames);
            var tabId = "result_table_" + Helper.JSONProcessor.TablesAdded;
            for(var i = 0; i < arrRecordsDefinition.length; i++)
            {
                var index = arrRecordsDefinition.indexOf(i);
                row.push(arrRecordsDefinitionNiceNames[index]);
            }
            rows.push(row);

            jsonResults.Records.forEach(element => {
                rows.push(element);
            });
            
            var html = "";
            html += '<table style="margin-bottom: 10px">';
            html += '<tr>';
            html += '<td>';
            html += '<text style="font-size: 25px"><span class="badge badge-dark" style="margin-right: 20px">' + jsonResults.ReceivedURLParameters.TABLE + '</span></text>';
            html += '</td>';
            html += '<td>';
            html += '<text style="font-size: 25px"><span class="badge badge-dark" style="margin-right: 20px">' + jsonResults.ReceivedURLParameters.OPTIONS + '</span></text>';
            html += '</td>';
            html += '<td>';
            html += '<button type="button" class="btn btn-outline-danger btn-sm" onclick="Helper.JSONProcessor.Events.OnClickDeleteTable(\'' + tabId + '\')">Eliminar</button>';
            html += '</td>';
            // html += '<td>';
            // html += '<button type="button" class="btn btn-outline-dark btn-sm" onclick="Helper.JSONProcessor.Events.OnClean()">Recuperar parametros</button>';
            // html += '</td>';
            html += '<td>';
            html += '<input type="hidden"></input>';
            html += '</td>';
            html += '</tr>';
            html += '</table>';
            
            if(rows.length > 1)
            {
                html += '<table class="table table-striped" style="margin-bottom: 20px">';
                rows.forEach(function(row, index){
                    if(index == 0)
                    {
                        html += '<thead class="thead-dark">';
                        html += '<tr>';
                        row.forEach(function(element){
                            html += '<th scope="col">';
                            html += element;
                            html += '</th>';
                        });
                        html += '</tr>';
                        html += '</thead>';
                        html += '<tbody>';
                    }
                    else
                    {
                        var pair = index % 2 == 0;
                        if (pair) html += '<tr class="table-light">';
                        if (!pair) html += '<tr class="table-secondary">';
                        row.forEach(function(element){
                            html += '<th scope="col">';
                            html += element;
                            html += '</th>';
                        });
                        html += '</tr>';
                    }
                });
                html += '</tbody></table>';
            }
            else
            {
                if(!Helper.Globals.IsEmpty(jsonResults.ERRORS) && jsonResults.ERRORS.length != 0)
                {
                    jsonResults.ERRORS.forEach(function(element){
                        html += '<div class="alert alert-danger" role="alert">';
                        html += element.err;
                        html += '</div>';
                    });
                }
                else
                {
                    html += '<div class="alert alert-warning" role="alert">';
                    html += "No se encontraron registros.";
                    html += '</div>';
                }
            }
            var div = document.createElement('div');
            div.id = tabId;
            div.style.marginBottom = "50px"
            div.innerHTML = html.trim();
            document.getElementById("tables_result").appendChild(div);
        },
        Events: {
            OnClean: function(){
                document.getElementById("tables_result").innerHTML = "";
            },
            OnAdd: function(){
                Helper.JSONProcessor.TablesAdded++;
                Helper.JSONProcessor.DrawTable(document.getElementById("jsonprocessor").value);
            },
            OnClickDeleteTable: function(tableId)
            {
                document.getElementById(tableId).remove();
            }
        }
    }
}